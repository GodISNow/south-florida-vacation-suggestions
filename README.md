# South Florida Vacation Suggestions

Ashish is going to South Florida!  
Where should he go?

## Miami to Key West

This is a mini itinerary for the drive from Miami to Key West. 
It includes stops along the way to Key West and a few kid friendly things to do!

**Grab a bite before going all the way down**  
Pinecrest Bakery  
99100 Overseas Hwy, Key Largo, FL 33037  
pinecrestbakery.com  
Note: Get croquetas(chicken, fish, ham), tequeño, a colada, and whatever catches your eye.

**Feed the Tarpon**  
Robbies  
77522 Overseas Hwy, Islamorada, FL 33036  
https://www.robbies.com/  
Note: Bring Cash  

**Grab some coffee on the way**  
Baby’s Coffee  
3180 US-1, Key West, FL 33040  
babyscoffee.com  

**Watch the Sunset**  
Mallory Square  
400 Wall St, Key West, FL 33040  
https://www.mallorysquare.com/  

**Cool Picture Spot**  
Southernmost Point of the Continental US  
Whitehead St &, South St, Key West, FL 33040  
cityofkeywest-fl.gov  

**See Cats at the Hemmingway house**  
The Ernest Hemingway Home and Museum  
907 Whitehead St, Key West, FL 33040  
hemingwayhome.com  

**Dinner**  
Conch Republic Seafood Company  
631 Greene St, Key West, FL 33040  
https://conchrepublicseafood.com/menu/  

**Dessert**  
Key West Key Lime Pie Co  
511 Greene St, Key West, FL 33040  
keywestkeylimepieco.com  

**People Watch on Duval Street**  
Sloppy Joe's Bar  
201 Duval St, Key West, FL 33040  
https://sloppyjoes.com/  
Note: That's the starting point, just walk around there.  

**Place from my story**  
Garden of Eden  
224 Duval St, Key West, FL 33040  
bullkeywest.org  
Note: Just so you can walk by it and laugh

## Miami Beach

**Breakfast**  
11th Street Diner  
1458 Ocean Dr, Miami Beach, FL 33139  
frontporchoceandrive.com  
Note: It's a normal Diner, but the food's good and it's o a perfect location
to walk to the beach.  

**Beach**  
Just a quick walk from 11th Street Diner heading East.
Go in the water and remember to bring towels.  
Note: Make sure you watch your belongings.

**Walk around the boardwalk**  
Around the beach theres a little boardwalk you can walk down.

**Walk around Lincoln Road**  
At night you can walk around Lincoln Road and just explore, theres a bunch of
cool little shops around.

**Dinner**  
Too many things here lol. Just depends.  
